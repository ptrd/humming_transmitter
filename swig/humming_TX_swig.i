/* -*- c++ -*- */

#define HUMMING_TX_API

%include "gnuradio.i"			// the common stuff

//load generated python docstrings
%include "humming_TX_swig_doc.i"

%{
#include "humming_TX/humming_TX.h"
%}


%include "humming_TX/humming_TX.h"
GR_SWIG_BLOCK_MAGIC2(humming_TX, humming_TX);
