# CMake generated Testfile for 
# Source directory: /home/sensor/SDR/gr-humming_TX/lib
# Build directory: /home/sensor/SDR/gr-humming_TX/build/lib
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
ADD_TEST(test_humming_TX "/bin/sh" "/home/sensor/SDR/gr-humming_TX/build/lib/test_humming_TX_test.sh")
