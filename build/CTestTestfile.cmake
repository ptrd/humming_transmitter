# CMake generated Testfile for 
# Source directory: /home/sensor/SDR/gr-humming_TX
# Build directory: /home/sensor/SDR/gr-humming_TX/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
SUBDIRS(include/humming_TX)
SUBDIRS(lib)
SUBDIRS(swig)
SUBDIRS(python)
SUBDIRS(grc)
SUBDIRS(apps)
SUBDIRS(docs)
