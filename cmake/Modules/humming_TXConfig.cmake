INCLUDE(FindPkgConfig)
PKG_CHECK_MODULES(PC_HUMMING_TX humming_TX)

FIND_PATH(
    HUMMING_TX_INCLUDE_DIRS
    NAMES humming_TX/api.h
    HINTS $ENV{HUMMING_TX_DIR}/include
        ${PC_HUMMING_TX_INCLUDEDIR}
    PATHS ${CMAKE_INSTALL_PREFIX}/include
          /usr/local/include
          /usr/include
)

FIND_LIBRARY(
    HUMMING_TX_LIBRARIES
    NAMES gnuradio-humming_TX
    HINTS $ENV{HUMMING_TX_DIR}/lib
        ${PC_HUMMING_TX_LIBDIR}
    PATHS ${CMAKE_INSTALL_PREFIX}/lib
          ${CMAKE_INSTALL_PREFIX}/lib64
          /usr/local/lib
          /usr/local/lib64
          /usr/lib
          /usr/lib64
)

INCLUDE(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(HUMMING_TX DEFAULT_MSG HUMMING_TX_LIBRARIES HUMMING_TX_INCLUDE_DIRS)
MARK_AS_ADVANCED(HUMMING_TX_LIBRARIES HUMMING_TX_INCLUDE_DIRS)

