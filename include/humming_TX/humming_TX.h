/* -*- c++ -*- */
/* 
 * Copyright 2016 <+YOU OR YOUR COMPANY+>.
 * 
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 * 
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */


#ifndef INCLUDED_HUMMING_TX_HUMMING_TX_H
#define INCLUDED_HUMMING_TX_HUMMING_TX_H

#include <humming_TX/api.h>
#include <gnuradio/sync_block.h>

namespace gr {
  namespace humming_TX {

    /*!
     * \brief <+description of block+>
     * \ingroup humming_TX
     *
     */
    class HUMMING_TX_API humming_TX : virtual public gr::sync_block
    {
     public:
      typedef boost::shared_ptr<humming_TX> sptr;

      /*!
       * \brief Return a shared_ptr to a new instance of humming_TX::humming_TX.
       *
       * To avoid accidental use of raw pointers, humming_TX::humming_TX's
       * constructor is in a private implementation
       * class. humming_TX::humming_TX::make is the public interface for
       * creating new instances.
       */
      static sptr make(const std::vector<int> data, int sampling_kHz, int period_ms, const std::vector<int> ratio_szo, int s_length_uS, int divider);

      virtual void set_data(const std::vector<int> data) = 0;
      virtual void set_divider(int divider) = 0;

      virtual void add_S(int bit) = 0;

      virtual int CRC_get(const std::vector<unsigned char> CRC_input) = 0;
      virtual unsigned int CCITT_Update(unsigned int init, unsigned int input) = 0;
    };

  } // namespace humming_TX
} // namespace gr

#endif /* INCLUDED_HUMMING_TX_HUMMING_TX_H */

