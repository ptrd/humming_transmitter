/* -*- c++ -*- */
/* 
 * Copyright 2016 <+YOU OR YOUR COMPANY+>.
 * 
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 * 
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef INCLUDED_HUMMING_TX_HUMMING_TX_IMPL_H
#define INCLUDED_HUMMING_TX_HUMMING_TX_IMPL_H

#include <humming_TX/humming_TX.h>
#include <queue>
#include <vector>


namespace gr {
  namespace humming_TX {

    class humming_TX_impl : public humming_TX
    {
     private:
      // parameters copied into the block fields
      int d_spf;
      int d_zb;
      int d_ob;
      int d_sb;

      // internal parameters used for internal computing
      int index;
      int current_bit;
      int samples;
      int d_div;

      // data structures for computation
      std::vector<int> Svec;
      std::queue< std::vector<int> > d_data;
      //std::vector<int> test_vector;
      std::queue<int> q;
      int frame_counter;

      // function declarization
      void set_data(const std::vector<int> data);
      void set_divider(int divider);
      void add_S(int bit);

      // CRC functions
      int CRC_get(const std::vector<unsigned char> CRC_input);
      unsigned int CCITT_Update(unsigned int init, unsigned int input);

     public:
      humming_TX_impl(const std::vector<int> data, int sampling_kHz, int period_ms, const std::vector<int> ratio_szo, int s_length_uS, int divider);
      ~humming_TX_impl();

      // Where all the action really happens
      int work(int noutput_items,
	       gr_vector_const_void_star &input_items,
	       gr_vector_void_star &output_items);
    };

  } // namespace humming_TX
} // namespace gr

#endif /* INCLUDED_HUMMING_TX_HUMMING_TX_IMPL_H */

