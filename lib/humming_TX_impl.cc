/* -*- c++ -*- */
/* 
 * Copyright 2016 <+YOU OR YOUR COMPANY+>.
 * 
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 * 
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gnuradio/io_signature.h>
#include "humming_TX_impl.h"
#define SINGLE_FRAME 3
#define SPLITTED_FRAME_START 4 // add S bit(s) at the beginning
#define SPLITTED_FRAME_MIDDLE 5 // don't add S bit
#define SPLITTED_FRAME_END 6 // add S bit(s) at the end
#define SPLITTED_FRAME_START_2_S 7

#define BIT_ZERO 0
#define BIT_ONE 1
#define BIT_S 3

namespace gr {
  namespace humming_TX {

    humming_TX::sptr
    humming_TX::make(const std::vector<int> data, int sampling_kHz, int period_ms, const std::vector<int> ratio_szo, int s_length_uS, int divider)
    {
      return gnuradio::get_initial_sptr
        (new humming_TX_impl(data, sampling_kHz, period_ms, ratio_szo, s_length_uS, divider));
    }

    /*
     * The private constructor
     */
    humming_TX_impl::humming_TX_impl(const std::vector<int> data, int sampling_kHz, int period_ms, const std::vector<int> ratio_szo, int s_length_uS, int divider)
      : gr::sync_block("humming_TX",
              gr::io_signature::make(0, 0, 0),
              gr::io_signature::make(1, 1, sizeof(float)))
    {
      // copies the parameters to object fields
      d_spf = sampling_kHz * period_ms;
      std::cout << "Number of samples per frames : " << d_spf << "\n";
      d_sb = (s_length_uS * d_spf / (period_ms * 1000) );
      std::cout<< "s_length_uS: " << s_length_uS << "\n";
      std::cout<< "d_spf: " << d_spf << "\n";
      std::cout<< "period_ms: " << period_ms << "\n"; 
      std::cout<< "period_ms * 1000: " << (period_ms * 1000) << "\n";
      std::cout << "S-bit sample width: \t" << d_sb << "\n";
      d_zb = d_sb / ratio_szo[0] * ratio_szo[1];
      std::cout << "Zero-bit sample width: \t" << d_zb << "\n";
      d_ob = d_sb / ratio_szo[0] * ratio_szo[2];
      std::cout << "One-bit sample width: \t" << d_ob << "\n";

      // constructs internal parameters
      index = 0;
      current_bit = 0;
      samples = 0;
      frame_counter = 1;
      set_divider(divider);
      set_data(data); //??

      // fill Svec vector with single S-bit containing protocol
      for (int i = 0; i < d_sb; i++) {
        Svec.push_back(0);
      }
      for (int i = d_sb; i < d_spf; i++) {
        Svec.push_back(1);
      }
    }

    /*
     * Our virtual destructor.
     */
    humming_TX_impl::~humming_TX_impl()
    {
    }

    void humming_TX_impl::add_S(int bit) {
      for (int i = 0; i < d_sb; i++) {
        q.push(bit);
        samples++;
      }
    }

    void humming_TX_impl::set_divider(int divider) {
      d_div = divider;
    }
    
    // This function compute CRC and append the vectors to internal data structure
    void humming_TX_impl::set_data(const std::vector<int> data) {
      // iterating over the data vector to copy the data in byte forms for CRC computation

      std::cout << "Data input it: " << std::endl;
      for(int i = 0; i <  data.size(); i++){
        std::cout << data[i];
      }
      std::cout << std::endl;

      int bytemasks [8] = {0x80, 0x40, 0x20, 0x10,
                           0x08, 0x04, 0x02, 0x01};
      std::vector<unsigned char> CRC_data;
      // iterate over 8 bits from data vector and translate it into unsigned char type
      for (int i = 0; i < data.size(); i+=8) {
        unsigned char byte = 0x00;
        for (int j = 0; j < 8; j++) {
          if (data[(i + j)] == 1) {
            byte |= bytemasks[j];
          }
        }
        CRC_data.push_back(byte);
      }
      
      // compute the CRC bits
      int16_t CRC = CRC_get(CRC_data);
      std::cout << "CRC Computed: \t" << std::bitset<16>(CRC) << ", Hex: 0x" << std::hex << CRC << std::dec << std::endl << "\n";

      // Appending CRC to data
      std::vector<int> data_CRC = data;
      int bitmasks [16] = {0x8000, 0x4000, 0x2000, 0x1000,
                           0x0800, 0x0400, 0x0200, 0x0100,
                           0x0080, 0x0040, 0x0020, 0x0010,
                           0x0008, 0x0004, 0x0002, 0x0001};
      std::cout <<"CRC Appended: \t";
      for (int i = 0; i < 16; i++) {
        if ((CRC & bitmasks[i]) == 0x0000) {
          data_CRC.push_back(0);
          std::cout<<"0";
        } else {
          data_CRC.push_back(1);
          std::cout<<"1";
        }
      }
      std::cout<<"\n\n";

      int bpf =  d_div;
      int total_bits_num = data_CRC.size();
      std::cout<< "Bits per frame:  " << bpf << " bits" << std::endl;
      std::cout<< "Total number of bits to send:  " << total_bits_num << " bits" << std::endl;

      // Append the original data vector + CRC vector and enqueue to internal data structure
      //if (d_div == 0 || d_div == 1) {
      if(bpf == 0 || bpf % 2 == 0 || bpf >= total_bits_num){
        data_CRC.insert (data_CRC.begin(), BIT_S); //put a flag at the beginning of the command vector indicating it is a single frame
        if(data_CRC.size() % 2 == 0){ // even number of bits, append only 1 S bit at the end
          data_CRC.insert (data_CRC.end(), BIT_S);
        }else{
          data_CRC.insert (data_CRC.end(), BIT_S);
          data_CRC.insert (data_CRC.end(), BIT_S);
        }
        d_data.push(data_CRC);

      } else { // split bits into different frames (cmd_vectors)
          std::vector<int> temp;
          int data_CRC_start_index = 0;
          int data_CRC_end_index = 0;
          int data_CRC_size = data_CRC.size();

          // take care of first frame
          temp.push_back(BIT_S);
          if(bpf == 1){ // the first frame should contain 2 S bits
            temp.push_back(BIT_S);
            total_bits_num -= bpf;
            data_CRC_end_index += bpf;

          }else{ // other odd number 3, 5, 7, 9 ...
            total_bits_num -= (bpf - 1);
            data_CRC_end_index += (bpf - 1);
          }

          for(int i=data_CRC_start_index; i < data_CRC_end_index; i++){
            temp.push_back(data_CRC[i]);
          }

          data_CRC_start_index = data_CRC_end_index;
          d_data.push(temp);
          temp.clear();

          // take care of the rest of frames
          while(total_bits_num > bpf){
            total_bits_num -= bpf;
            data_CRC_end_index += bpf;

            for(int i=data_CRC_start_index; i < data_CRC_end_index; i++){
              temp.push_back(data_CRC[i]);
            }

            data_CRC_start_index = data_CRC_end_index;
            d_data.push(temp);
            temp.clear();
          }

          //take care of the last frame
          data_CRC_end_index += total_bits_num; // add the remainder
          for(int i=data_CRC_start_index; i < data_CRC_end_index; i++){
            temp.push_back(data_CRC[i]);
          }

          temp.push_back(BIT_S);
          if(total_bits_num % 2 != 0){
            temp.push_back(BIT_S);
          }

          d_data.push(temp);
          temp.clear();
      }
       
      // printing the contents of the data_CRC for checking
      std::cout<<"Data+CRC enqueued: ";
      for (int k = 0; k < data_CRC.size(); k++) {
        std::cout<< data_CRC[k];
      }
      std::cout<<"\n";
       std::cout<<"d_data length: " << d_data.size();

      std::cout<<"\n";
    }

    /* CRC Codes */
    int humming_TX_impl::CRC_get(const std::vector<unsigned char> CRC_input) {
      int SW_Results = 0xFFFF;   // can be 0xFFFF
      for (int i = 0; i < CRC_input.size(); i++) {
        // First input lower byte
        SW_Results = CCITT_Update(SW_Results, CRC_input[i] & 0xFF);
        // Then input upper byte
        SW_Results = CCITT_Update(SW_Results, (CRC_input[i] >> 8) & 0xFF);
      }
      return SW_Results;
    }

    // CRC16 Software Algorithm - CCITT CRC 16 Code
    unsigned int humming_TX_impl::CCITT_Update(unsigned int init, unsigned int input) {
      unsigned int CCITT = (unsigned char)(init >> 8)|(init << 8);
      CCITT ^= input;
      CCITT ^= (unsigned char)(CCITT & 0xFF) >> 4;
      CCITT ^= (CCITT << 8) << 4;
      CCITT ^= ((CCITT & 0xFF) << 4) << 1;
      return CCITT;
    }

    int
    humming_TX_impl::work(int noutput_items,
			  gr_vector_const_void_star &input_items,
			  gr_vector_void_star &output_items)
    {

        //static int frame_counter = 1;
        float *out = (float *) output_items[0];

        // Do <+signal processing+>
        for (int i = 0; i < noutput_items; i++) {
          /* DATA BUFFERING */
          if (!d_data.empty()) {                               //d_data[0] != 2
            while (!d_data.empty()) {
              std::cout<<"enqueue \n";
              std::vector<int> command = d_data.front();
              d_data.pop();
              if (command[0] != 2) {
                // get the size of the data vector
                int vec_len = command.size();
                // reset the bit counter so that next one starts at one
                current_bit = 0;
                // for each element from the data vector
                std::cout << "Start add to q: ";
                //std::cout << "command is: ";
                for (int n = 0; n < vec_len; n++) {
                  //std::cout << command[n];
                  switch(command[n]){
                    case BIT_S:
                      for (int j = 0; j < d_sb; j++) {
                        q.push(current_bit);
                        samples++;
                      }
                      std::cout << "S";
                      break;

                    case BIT_ZERO:
                      for (int j = 0; j < d_zb; j++) {
                        q.push(current_bit);
                        samples++;
                      }
                      std::cout << "0";
                      break;

                    case BIT_ONE:
                      for (int j = 0; j < d_ob; j++) {
                        q.push(current_bit);
                        samples++;
                      }
                      std::cout << "1";
                      break;
                  }
                  current_bit = (current_bit + 1) % 2;  // toggle polarity bit for each bits
                }
                std::cout << "\n";

                // fill the remaining samples for frame with 1's
                for (int n = 0; n < (d_spf - samples); n++) {
                  q.push(1);
                }
                samples = 0;
              }
            }
          }

          /* OUTPUT STREAM */
          if ( (index > 0) || (q.empty()) ) {
            //std::cout<<"default: " << std::dec << (index + 1) << " " << Svec[index] << "\n";
            out[i] = Svec[index];
            index++;
            if (index >= d_spf) {
              index = 0;
              
              //send some random data every 5 frames for testing purpose
              if(frame_counter % 5 == 0){
                int myints[] = {0, 1, 0, 1, 0, 1, 0, 0};
                std::vector<int> temp_v;
                int arr_len = sizeof(myints) / sizeof(int);
                for(int i = 0; i< arr_len; i++){
                  temp_v.push_back(myints[i]);
                }
                std::cout << "frame_counter is: " << frame_counter << std::endl;
                set_data(temp_v);
                frame_counter = 1;
              }else{
                frame_counter++;
              }

            }
          } else {
            //std::cout<<"data: " << std::dec << q.front() << "\n";
            out[i] = (float)q.front();
            q.pop();
          }
        }

        // Tell runtime system how many output items we produced.
        return noutput_items;
    }

  } /* namespace humming_TX */
} /* namespace gr */

